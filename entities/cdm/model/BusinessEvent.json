{
  "classifierPath" : "meta::pure::metamodel::type::Class",
  "content" : {
    "_type" : "class",
    "name" : "BusinessEvent",
    "package" : "cdm::model",
    "properties" : [ {
      "multiplicity" : {
        "lowerBound" : 1
      },
      "name" : "primitives",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The elemental component(s) that specify the lifecycle events. Each of the primitive/elemental components listed as part of the PrimitiveEvent class has distinctive features that allow to specify the lifecycle event, either by itself or in combination with some other of such components."
      } ],
      "type" : "cdm::model::PrimitiveEvent"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "intent",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The intent attribute is meant to be specified when the event qualification cannot be programmatically inferred from the event features. As a result it is only associated with those primitives that can give way to such ambiguity, the quantityChange being one of those. An example of such is a reduction in the trade notional, which could be interpreted as either a trade correction (unless a maximum period of time post-event is specified as part of the qualification), a partial termination or a portfolio rebalancing in the case of an equity swap. On the other hand, an event such as the exercise is not expected to have an associated intent as there should not be ambiguity."
      } ],
      "type" : "cdm::model::IntentEnum"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "functionCall",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "This is placeholder concept for a function call into a calculation that will return an outcome. This concept needs to be further firmed out."
      } ],
      "type" : "String"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "eventQualifier",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The CDM event qualifier, which corresponds to the outcome of the isEvent qualification logic which qualifies the lifecycle event as a function of its features (e.g. PartialTermination, ClearingSubmission, Novation, ...)."
      } ],
      "type" : "String"
    }, {
      "multiplicity" : {
        "lowerBound" : 1,
        "upperBound" : 1
      },
      "name" : "eventDate",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Specifies the date on which the event is taking place. This date is equal to the trade date in the case of a simple execution.  However it can be different from the trade date, for example in the case of a partial termination."
      } ],
      "type" : "Date"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "effectiveDate",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The date on which the event contractually takes effect, when different from the event date."
      } ],
      "type" : "Date"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "eventEffect",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The set of effects associated with the lifecycle event, i.e. generated cashflows, contracts (from, say, novation events), listed products (from, say, a bond option exercise event) values and more. Those are represented through a set of globalKey references. This attribute is optional in order to provide implementers with the ability not to make use of this feature."
      } ],
      "type" : "cdm::model::EventEffect"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "workflowEventState",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The event workflow information, i.e. the workflow status, the associated comment and the partyCustomisedWorkflow which purpose is to provide the ability to associate custom workflow information to the CDM."
      } ],
      "type" : "cdm::model::WorkflowStepState"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "tradeWarehouseWorkflow",
      "type" : "cdm::model::TradeWarehouseWorkflow"
    } ],
    "stereotypes" : [ {
      "profile" : "cdm::model::metadata",
      "value" : "key"
    } ],
    "taggedValues" : [ {
      "tag" : {
        "profile" : "meta::pure::profiles::doc",
        "value" : "doc"
      },
      "value" : "A business event represents a life cycle event of a trade and consists of a series of primitive events. The combination of the state changes results in a qualifiable life cycle event. An example of a Business Event is a PartialTermination which is a defined by a quantity change primitive event."
    } ]
  }
}